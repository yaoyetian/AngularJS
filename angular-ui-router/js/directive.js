//directive 指令 
routerApp.directive('hello', function(){
	return{ 
		restrict: 'AEMC',															//restrict 匹配模式有四个选项
		template:'<div class="hellanglular">hello everyone!!!</div>',				//A属性(默认) E元素 (推荐)
		replace:true   																//M注释 C 类class  不常用
	 }
	// return{ 
	// 	restrict: 'A',																 
	// 	templateUrl:'tpls/luyou-1.html',										//可以用templateUrl引用模版页面
	// 	replace:true   																 
	// },
});

// <!--指令与控制器之间的交互-->


routerApp.controller('zhiling1', ['$scope', function($scope){
	$scope.loadDate1=function(){
		console.log('加载数据。。。。。。。。。。。。。1111111111111111111111');
	}
}])
routerApp.controller('zhiling2', ['$scope', function($scope){
	$scope.loadDate2=function(){
		console.log('加载数据。。。。。。。。。。。。。2222222222222222222222');
	}
}])
routerApp.controller('zhiling3', ['$scope', function($scope){
	$scope.loadDate3=function(){
		console.log('加载数据。。。。。。。。。。。。。33333333333333333333333');
	}
}])
routerApp.controller('zhiling4', ['$scope', function($scope){
	$scope.loadDate4=function(){
		console.log('加载数据。。。。。。。。。。。。。444444444444444444444444');
	}
}])
//这个指令可以复用
routerApp.directive('laoder',function(){
	return {
		restrict:'AE', 		 
		link: function(scope, element, attrs) {//
			element.bind('mouseenter', function() {
				//这两种方法一样的
				//scope.loadDate();				//方法一
				//scope.$apply('loadDate()');	//方法二
				
				//多处可复用
				scope.$apply(attrs.zloader);
			});
		}
	};
});


//////////////////
// expander指令 //
//////////////////
routerApp.controller('expander', function($scope){
	$scope.title='点击展开',
	$scope.text='内部内容'
})
routerApp.directive('expander',function(){//获取expander指令
	return {
		restrict:'EA',//元素选择 &&属性选择
		replace :true,
		transclude:true,
		scope : {
			title : '=expanderTitle'
		},
		template : '<div>'
				 + '<div class="title" ng-click="toggle()">{{title}}</div>'
				 + '<div class="body" ng-show="showMe" ng-transclude></div>'
				 + '</div>',
		link: function(scope, element, Attrs ) {
			 scope.showMe = false;
			 scope.toggle = function(){
			 	scope.showMe = !scope.showMe
			 }
		}
	}
});

