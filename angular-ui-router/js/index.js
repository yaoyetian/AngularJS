var routerApp = angular.module('AngulJS-router', ['ui.router']);  //ceshi1模块名称  //依赖 angularui 
routerApp.config(function($stateProvider, $urlRouterProvider) {
    console.log(123);
    $urlRouterProvider.otherwise('/index');
    $stateProvider        
        .state('index', {
            url: '/index',//
            views: {
                '': {
                    templateUrl: 'tpls/index.html'//加载代码片段
                },
                'topbar@index': {//以@的方式把ui-view tpls/index.html 里的分割页面组合
                    templateUrl: 'tpls/topbar.html'
                },
                'main@index': {//以@的方式把ui-view tpls/index.html 里的分割页面组合
                    templateUrl: 'tpls/home.html'
                }
            }
        })
        .state('index.permission', {
            url: '/permission',
            views: {
                'main@index': {
                    template: '这里是权限管理'
                }
            }
        })
        .state('index.report', {
            url: '/report',
            views: {
                'main@index': {
                    template: '这里是报表管理'
                }
            }
        })
        .state('index.settings', {
            url: '/settings',
            views: {
                'main@index': {
                    template: '这里是系统设置'
                }
            }
        })
});
